using Godot;
using System;

public partial class Saw : Node2D
{
    private DamageableZone damageableZone;
    public AnimatedSprite2D Sprite;
    //40 - 250
    [Export] public float Speed;
    [Export] public int Damage;
    public Vector2 FirstPosition;
    public Vector2 SecondPosition;
    private Vector2 currentPosition;
    public Vector2 velocity;
    private float moveDelta = 0;
    public Status status;
    public Node2D SecondPositionNode;

    private bool canRotate = true;

    private double timeleft = 0.4;

    public override void _Ready()
    {
        damageableZone = GetChild<DamageableZone>(0);
        Sprite = GetChild<AnimatedSprite2D>(1);
        SecondPositionNode = GetChild<Node2D>(2);
        damageableZone.Damage = Damage;
        velocity = Vector2.Zero;
        FirstPosition = GlobalPosition;
        SecondPosition = SecondPositionNode.GlobalPosition;

        if(status == Status.Stopped)
        {
            damageableZone.SetCollisionMaskValue(1 , false);
            Sprite.Play("Off");
        }
        else
        {
            damageableZone.SetCollisionMaskValue(1 , true);
            velocity = (FirstPosition - SecondPosition).Normalized();
            Sprite.Play("On");
        }  
    }

    public override void _PhysicsProcess(double delta)
    {
        timeleft -= delta;
        if(timeleft < 0)
        {
            timeleft = 0.5;
            canRotate = true;
        }

        currentPosition = Position;

        if(status == Status.Stopped)
            return;

        if(currentPosition.DistanceTo(SecondPosition) < 2f || currentPosition.DistanceTo(FirstPosition) < 2f)
        {  
            Rotate();
        }
        var distance = Speed * (float)delta;
        Position += velocity.Normalized() * distance;
    }

    public void Rotate()
    {
        if(canRotate)
        {
            Speed *= -1;
            canRotate = false;
        }       
    }

    public void SwitchPower()
    {
        if(status == Status.Stopped)
        {
            status = Status.Working;
            damageableZone.SetCollisionMaskValue(1 , true);
            Sprite.Play("On");
        }
        else
        {
            status = Status.Stopped;
            damageableZone.SetCollisionMaskValue(1 , false);
            Sprite.Play("Off");
        }       
    }

    public enum Status
    {
        Working,
        Stopped
    }
}
