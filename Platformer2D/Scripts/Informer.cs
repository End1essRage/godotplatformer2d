using Godot;
using System;

public partial class Informer : Node
{
    private Player player;
    private HUD hud;
    public override void _Ready()
    {
        base._Ready();
        
        player = GetParent<Player>();
        hud = player.GetChild<HUD>(4);
        player.OnStatusChanged += UpdateHud;
    }

    public void UpdateHud()
    {
        var info = player.GetInfo();
        hud.ChangeLabel(FormatLabelText(info));
        hud.ChangePanelColor();
    }

    public string FormatLabelText(PlayerInfo info)
    {
        return string.Format(" Points = {0} \n hp = {1}", info.Points, info.Hp);
    }
}
