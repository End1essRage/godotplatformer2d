using Godot;
using System;

public partial class HUD : CanvasLayer
{
    private Label pointsLabel;
    private Panel panel;

    public override void _Ready()
    {
        pointsLabel = GetChild<Label>(1);
        panel = GetChild<Panel>(0);
    }

    public void ChangeLabel(string text)
    {
        pointsLabel.Text = text;
    }

    public void ChangePanelColor()
    {   
    }
}
