using Godot;
using System;


public class PlayerInfo
{
    public int Points { get; set; }
    public int Hp { get; set; }
}

public class Timer
{
	private double Amount{ get; set; }
	private double CurrentAmount{ get; set; }
	public bool IsAutoRestart{get;set;}
	public Timer(double amount, bool isAutoRestart)
	{
		Amount = amount;
		CurrentAmount = amount;
		IsAutoRestart = isAutoRestart;
	}

	public void Restart()
	{
		CurrentAmount = Amount;
	}

	//returns true when countdown ended
	public bool CountDown(double delta)
	{
		CurrentAmount -= delta;
		if(CurrentAmount < 0)
		{
			if(IsAutoRestart)
				Restart();
			return true;
		}
		else
		{
			return false;
		}	
	}
}


public partial class Player : CharacterBody2D
{
	#region variables
	[Export] public const float Speed = 200.0f;
	[Export] public const float JumpVelocity = -350.0f;
	[Export] public const float ImpactPower = 350.0f;
	[Export] public int HitPoints = 3;
	[Export] public double immortalTime = 1.2;
	[Export] public double impactTime = 0.4;

	[Signal]
	public delegate void OnDeathEventHandler(int points);
	[Signal]
	public delegate void OnStatusChangedEventHandler();
	public Respawner Respawn;
	public int Points = 0;
	public float gravity = ProjectSettings.GetSetting("physics/2d/default_gravity").AsSingle();
	public AnimatedSprite2D animatedSprite;
	public bool fJumpCharge = false;
	public Skin skin;
	private bool isDamageable = false;
	private bool isImpacted = false;
	private Timer timmortalTimer;
	private Timer timpactTimer;
	private Status currentState;

	#endregion


	public enum Skin
    {
        NinjaFrog,
        PinkMan,
        VirtualGuy,
        MaskDude
    }
	public enum Status
	{
		Impacted,
		Immortal,
		Normal
	}
	public PlayerInfo GetInfo()
	{
		var info = new PlayerInfo();
		info.Points = Points;
		info.Hp = HitPoints;
		return info;
	}
	public override void _Ready()
	{
		timmortalTimer = new Timer(immortalTime, true);
		timpactTimer = new Timer(impactTime, true);
		animatedSprite.Play();
		currentState = Status.Immortal;
		EmitSignal(SignalName.OnStatusChanged);
	}

	public override void _PhysicsProcess(double delta)
	{
		if(isImpacted)
		{
			isImpacted = !timpactTimer.CountDown(delta);
		}

		if(isDamageable == false)
		{
			isDamageable = timmortalTimer.CountDown(delta);
		}

		StateMachine(isImpacted, !isDamageable);
		Vector2 velocity = Velocity;	
		Gravity(ref velocity, delta);
		RechargeJump();
		if(currentState != Status.Impacted)
		{
			Movement(ref velocity);
		}	
		SetAnimation(velocity);		
		Velocity = velocity;
		MoveAndSlide();
	}

	private void StateMachine(bool impacted, bool immortal)
	{
		if(impacted)
		{
			currentState = Status.Impacted;
			return;
		}
		if(immortal)
		{
			currentState = Status.Immortal;
			return;
		}
		currentState = Status.Normal;
	}

	public void AddPoints(int amount)
	{
		Points += amount;
		EmitSignal(SignalName.OnStatusChanged);
	}

	public void GetDamage(int damage, float position)
	{
		if(currentState == Status.Immortal || currentState == Status.Impacted)
			return;

		HitPoints -= damage;
		EmitSignal(SignalName.OnStatusChanged);
		if(HitPoints < 1)
			Die();

		HandleDamage(position);
	}

	public void HandleDamage(float position)
	{
		isDamageable = false;
		isImpacted = true;
		Vector2 velocity = Velocity;
		velocity.Y = JumpVelocity * 0.7f;
		velocity.X = GlobalPosition.X <= position ? -ImpactPower : ImpactPower;
		Velocity = velocity;
		MoveAndSlide();
	}
	public void Spawn(Skin skin, int points)
	{
		var skinInstance = GD.Load<PackedScene>(GetSkinSpritePath(skin)).Instantiate();
		this.skin = skin;
		animatedSprite = skinInstance as AnimatedSprite2D;
		Points = points;
		AddChild(skinInstance);
	}
    public string GetSkinSpritePath(Skin skin)
    {
        string folderPath = "res://Items/AnimatedSprites/";
        string skinName = "";
        switch(skin)
        {
            case Skin.NinjaFrog:
                skinName = "animated_sprite_ninja_frog.tscn";
                break;
            case Skin.PinkMan:
                skinName = "animated_sprite_pink_man.tscn";
                break;
            case Skin.VirtualGuy:
                skinName = "animated_sprite_vitual_guy.tscn";
                break;
            case Skin.MaskDude:
                skinName = "animated_sprite_mask_dude.tscn";
                break;
        }
        return folderPath + skinName;
    }

	private void Die()
	{
		EmitSignal(SignalName.OnDeath, Points);
	}

	private void Gravity(ref Vector2 velocity, double delta)
	{
		// Add the gravity.
		if (!IsOnFloor())
		{
			velocity.Y += gravity * (float)delta;
		}
	}

	private void SetAnimation(Vector2 velocity)
	{
		if(currentState == Status.Impacted)
		{
			animatedSprite.Animation = "TakeDamage";
			return;
		}

		if(currentState == Status.Immortal)
		{
			Modulate = new Color(1,0,0, 0.4f);
		}
		else
		{
			Modulate = new Color(1,1,1);
		}

		if (!IsOnFloor())
		{
			animatedSprite.Animation = velocity.Y >= 0 ? "Falling" : fJumpCharge ? "Jump" : "SecondJump";
		}		
		else
		{
			animatedSprite.Animation = velocity.X == 0 ? "Idle" : "Run";
		}	
	}

	private void RechargeJump()
	{
		if(IsOnFloor())
		{
			fJumpCharge = true;
		}
	}

	private void Movement(ref Vector2 velocity)
	{	
		// Handle Jump.
		if (!Input.IsActionPressed("MoveDown") && Input.IsActionJustPressed("Jump") && IsOnFloor())
		{
			velocity.Y = JumpVelocity;
		}

		//SecondJump
		if(Input.IsActionJustPressed("Jump") && !IsOnFloor() && fJumpCharge)
		{
			fJumpCharge = false;
			velocity.Y = JumpVelocity;
		}

		//JumpDown
		if(Input.IsActionPressed("MoveDown") && Input.IsActionPressed("Jump"))
		{
			//Отключение видимости коллизий односторонних платформ
			SetCollisionMaskValue(3,false);
		}
		else
		{
			SetCollisionMaskValue(3,true);
		}

		Vector2 direction = new Vector2();
		if(Input.IsActionPressed("MoveLeft"))
		{
			direction.X = -1f;
			animatedSprite.FlipH = true;
		}
		else if(Input.IsActionPressed("MoveRight"))
		{
			direction.X = 1f;
			animatedSprite.FlipH = false;
		}
		else
		{
			direction.X = 0f;
		}

		if (direction != Vector2.Zero)
		{
			velocity.X = direction.X * Speed;
		}
		else
		{
			velocity.X = Mathf.MoveToward(Velocity.X, 0, Speed);
		}
	}
}

