using Godot;
using System;

public partial class DamageableZone : Area2D
{
    public CollisionShape2D CollisionShape;
    public int Damage;

    public override void _Ready()
    {
        CollisionShape = GetChild<CollisionShape2D>(0);
    }

    public void OnBodyEntered(Node2D body)
    {
        body.Call("GetDamage", Damage, GlobalPosition.X);
    }
}
