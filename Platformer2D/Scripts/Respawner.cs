using Godot;
using System;

public partial class Respawner : Node2D
{
    [Export] public Player.Skin typeSkin;
    public PackedScene player;
    public Player playerInstance;
    [Export] public bool IsStart;

    public override void _Ready()
    {
        player = GD.Load<PackedScene>("res://Items/Objects/player.tscn");  
        if(IsStart)
        {
            Spawn(0);
        }         
    }

    private void Spawn(int points)
    {
        playerInstance = player.Instantiate() as Player;
        playerInstance.Spawn(typeSkin, points);
        playerInstance.Respawn = this;
        playerInstance.Position = new Vector2(0,0); 
        Subscribe();
        AddChild(playerInstance);    
    }
    public void Respawn(int points)
    {
        playerInstance.QueueFree();
        Spawn(points);
    }
    public void Subscribe()
    {
        typeSkin = playerInstance.skin;
        playerInstance.OnDeath += Respawn;
    }
    public void UnSubscribe()
    {
       playerInstance.OnDeath -= Respawn; 
    }

    public void ChangeRespawn(Respawner resp)
    {
        UnSubscribe();
        playerInstance.Respawn = resp;
        resp.Subscribe();
    }
}
