using Godot;
using System;

public partial class CheckPoint : Respawner
{
    private AnimatedSprite2D sprite;
    private Area2D area;
    private bool isActivated = false;
    public override void _Ready()
    {
        base._Ready();
        sprite = GetNode<AnimatedSprite2D>("CheckPoint");
        area = GetNode<Area2D>("Area2D");
        sprite.Play("Default");
        
    }

    public void OnBodyEntered(Node2D body)
    {
        if(isActivated)
            return;

        if(body is Player)
        {
            sprite.AnimationLooped += Finished;
            sprite.Play("Activating");
            isActivated = true;
            playerInstance = body as Player;
            playerInstance.Respawn.ChangeRespawn(this);
        }
    }

    public void Finished()
    {
        GD.Print("looping");
        if(sprite.Animation.ToString() == "Activating")
        {
            sprite.Play("Activated");
            sprite.AnimationLooped -= Finished;
        }          
    }
}
