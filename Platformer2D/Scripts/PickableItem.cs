using Godot;
using System;

public partial class PickableItem : Area2D
{
    [Export]public FruitType Type = FruitType.Apple;
    [Signal]public delegate void OnFruitPickedUpEventHandler(int award);
    private int award = 0;
    private AnimatedSprite2D sprite;
    public override void _Ready()
    {
        sprite = GetNode<AnimatedSprite2D>("Sprite");
        if(Type == FruitType.Random)
        {
            var rnd = new Random();
            Type = (FruitType)rnd.Next(0, 7);
        }
        string type = "";
        switch(Type)
        {
            case FruitType.Apple:
                type = "Apple";
                award = 1;
                break;
            case FruitType.Banana:
                type = "Banana";
                award = 1;
                break;
            case FruitType.Cherry:
                type = "Cherry";
                award = 1;
                break;
            case FruitType.Kiwi:
                type = "Kiwi";
                award = 1;
                break;
            case FruitType.Melon:
                type = "Melon";
                award = 1;
                break;
            case FruitType.Orange:
                type = "Orange";
                award = 1;
                break;
            case FruitType.Pineapple:
                type = "Pineapple";
                award = 1;
                break;
            case FruitType.Strawberry:
                type = "Strawberry";
                award = 1;
                break;
        }
        sprite.Play(type);
    }
    public void OnBodyEntered(Node2D body)
    {
        sprite.Play("Picked");
        EmitSignal(SignalName.OnFruitPickedUp, award);
        body.Call("AddPoints", award);
    }

    public void OnSpriteAnimationFinished()
    {
        QueueFree();
    }
}

public enum FruitType
{
    Apple,
    Banana,
    Cherry,
    Kiwi,
    Melon,
    Orange,
    Pineapple,
    Strawberry,
    Random
}
